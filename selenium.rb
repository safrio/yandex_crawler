require '/home/user/sandbox/parsers/fish_generator'
require '/home/user/sandbox/parsers/rucaptcha/rucaptcha_api'
require 'capybara/dsl'
require 'selenium-webdriver'
require 'headless'
require 'mechanize-random-agent'
require 'google_drive'
require 'yaml'
require 'csv'

class SeleniumCrawler
  include Capybara::DSL

  def initialize

    Capybara.run_server = false
    Capybara.current_driver = :selenium
    Capybara.app_host = "https://passport.yandex.ru/registration/mail?from=mail&origin=home_v14_ru&retpath=https%3A%2F%2Fmail.yandex.ru"
    Capybara.default_max_wait_time = 6

    @@headless = Headless.new
    at_exit do
      @@headless.destroy
    end
    @@headless.start if Capybara.current_driver == :selenium


    _proxy = "socks5://localhost:9050"

    proxy = Selenium::WebDriver::Proxy.new(
      :http     => _proxy,
      :ftp      => _proxy,
      :ssl      => _proxy
    )

    caps = Selenium::WebDriver::Remote::Capabilities.ie(:proxy => proxy)

    m = Mechanize.new
    _user_agent = m.get_random_user_agent
    _user_data_dir = '~/.config/google-chrome/Default'
    @@driver = Selenium::WebDriver.for :chrome, :desired_capabilities => caps, :switches => %W[--user-agent=#{_user_agent}]

    @@driver.manage.delete_all_cookies

  end

end

class YandexCrawler < SeleniumCrawler


  def fill (xpath, value)
    input = @@driver.find_element :xpath => xpath;
    input.send_keys(value);
    sleep 0.05
  end


  def get_captcha(captcha_image_file)
    api = RucaptchaApi.new '98762331c2b25bc7122e04482bf34156'
    path_to_captcha = File.expand_path captcha_image_file
    captcha_id = api.send_captcha_for_solving path_to_captcha, params: {phrase: 0, language: 1, regsense: 1}
    begin
      solved_captcha = api.get_solved_captcha captcha_id 
    rescue
      p "Can't GET CAPTCHA"
      return
    end
  end


  def crawl
    _CONFIG = YAML.load_file('/home/user/sandbox/parsers/config.yml')

    system(_CONFIG['newnim'])
    sleep 1.5

    @@driver.get "https://passport.yandex.ru/registration/mail?from=mail&origin=home_v14_ru&retpath=https%3A%2F%2Fmail.yandex.ru"

    fish = FishGenerator.new()
    p fish.name + ' ' + fish.surname

    fill('//*[@id="firstname"]', fish.name)
    begin
      fill('//*[@id="surname"]', fish.surname)
    rescue
      fill('//*[@id="lastname"]', fish.surname)
    end      

    login = @@driver.find_element :xpath => '//*[@id="login"]'
    login.click
    login.send_keys :arrow_down

    @@wait.until {
      @@driver.find_element :xpath => '//*[@id="nb-1"]/body/div/div/form/div[3]/ol'
    }

    @@driver.execute_script "$('.login__suggestedLogin')[0].click()"

    password = fish.password

    @@driver.execute_script "$('#password').val('#{password}')"
    sleep 0.5
    @@driver.execute_script "$('#password_confirm').val('#{password}')"
    sleep 0.5

    havent_phone = @@driver.find_element :xpath => '//*[@id="nb-1"]/body/div/div/form/div[6]/div[1]/label[2]'
    havent_phone.click
        sleep(0.3)

    @@driver.execute_script "$('#hint_question_id').trigger('keydown').val('3').trigger('keyup')"
        sleep(0.3)
    @@driver.execute_script "$('#ui-id-1 li:nth-last-child(2) a').click()"
        sleep(0.3)

    fill('//*[@id="hint_answer"]', '620000')

    login_input = @@driver.find_element :xpath => '//*[@id="login"]'
    login = login_input.attribute("value")
    p login

    screenshot_image_filename = "#{_CONFIG['screenshots_path']}/#{login}.png"
    captcha_image_filename = "#{_CONFIG['screenshots_path']}/#{login}_captcha.png"
    @@driver.save_screenshot screenshot_image_filename

    captcha_img = @@driver.find_element :xpath, '//*[@id="nb-1"]/body/div/div/form/div[6]/div[3]/div[4]/div[4]/div[1]/div[1]/img'

    system("convert -crop 212x74+#{captcha_img.location.x}+#{captcha_img.location.y} #{screenshot_image_filename} #{captcha_image_filename}")

    captcha_answer = get_captcha captcha_image_filename
    p captcha_answer.force_encoding('utf-8')

    fill('//*[@id="answer"]', captcha_answer.force_encoding('utf-8'))

    submit = @@driver.find_element :xpath => '//*[@id="nb-5"]/span/span'
    submit.click

    sleep 7

    CSV.open("/home/user/sandbox/parsers/_to_parse/yandex.ru/file_new.csv", "a") do |csv|
      csv << [login, fish.password, fish.name, fish.surname, 3, 620000, Time.now]
    end

    result_screenshot_image_filename = "#{@@screenshots_path}/#{login}_.png"
    @@driver.save_screenshot result_screenshot_image_filename

    @@driver.close
    @@headless.stop if Capybara.current_driver == :selenium

  end
end


YandexCrawler.new.crawl